#!/bin/bash

#This file run all the flow (Spring Boot App and Grafana)

clear

echo -e "Stopping and Removing Services Containers!"
docker-compose -f ./docker-compose.yml down

echo -e "Running Services Containers!"
docker-compose -f ./docker-compose.yml up -d

echo -e "Stopping Spring Boot Application!"
killall -9 java
PID=$(ps  aux| grep java | awk -F 'spring-boot:run' '{print $1}' | head -n1 | awk -F $USER '{print $2}' | awk -F ' ' '{print $1}')
kill -9 $PID
echo -e "Killed Process PID=$PID"
# fuser -k 8080/tcp

echo -e "Running Spring Boot Application..."
mvn -f ./logs/ spring-boot:run &

sleep 20

echo -ne '\n' | echo -e "Spring Boot Application Started!"

echo -e "Stopping and Removing Client Containers!"
docker-compose -f ./client/docker-compose.yml down

echo -e "Running Client Containers!"
docker-compose -f ./client/docker-compose.yml up -d


