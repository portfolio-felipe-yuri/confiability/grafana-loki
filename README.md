# Grafana and Grafana Loki
This project teach how to install and configure Grafana, Grafana Loki and Prometheus.


## Requirements
- Spring Boot Application
- Data Base (Postgres)
- Prometeus
- Grafana
- Grafana Loki 
- Docker
- Docker Compose
- Opperation System Linux
- Linux Terminal

## How to Install Grafana 

1. Open the file 'docker-compose.yml' and fill in the settings below.
   
    ```yaml
    grafana-api-cursos:

    image: grafana/grafana
    user: $SUID:$SGID
    container_name: grafana-api-cursos
    volumes:
        - ./grafana:/var/lib/grafana
    restart: unless-stopped
    ports:
        - 3000:3000
    networks:
        - monit
    ```
2. Run the file 'run.sh' to up database and grafana.
   
    ```bash
    bash run.sh
    ```
    You should see the image below.

    ![Imgurl](docs/images/img-started.jpg)

3. Now, the grafana is running. Acess the url below to test.

    ```url
    http://localhost:3000/
    ```
    You should see the image below.

    ![Imgurl](docs/images/img-login.jpg)

4. Sign in with credentials below.

    ```yaml
    login:  admin
    pass:   admin
    ```
    In next step, you will have to change the credentials and finally you should see the image below.

    ![Imgurl](docs/images/img-dashboard.jpg)

5. Create the file '.env' in root folder, take the result of the command below and fill in the variables.
    
    ```bash
    getent passwd $USER
    ```

    ![Imgurl](docs/images/img-user-code.jpg)

    Like this
    
    ```env
    SUID=1000
    SGID=1000
    ```

6. Change the owner of project folders to the grafana user using the command below.
    ```bash
    sudo chown $USER: -R grafana
    ```

## Install Prometheus

1. Add the content in file 'docker-compose.yml'
    ```yml
    prometheus-api-cursos:
        image: prom/prometheus:latest
        user: "root:root"
        container_name: prometheus-api-cursos
        restart: unless-stopped
        volumes:
        - ./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
        - ./prometheus/prometheus_data:/prometheus
        command:
        - '--config.file=/etc/prometheus/prometheus.yml'
        - '--storage.tsdb.path=/prometheus'
        - '--web.console.libraries=/etc/prometheus/console_libraries'
        - '--web.console.templates=/etc/prometheus/consoles'
        - '--web.enable-lifecycle'
        ports:
        - 9090:9090
        networks:
        - monit
    ```
2. Create the file './prometheus/prometheus.yml' with the content below.
    ```yml
        global:
            scrape_interval: 15s

        scrape_configs:

        - job_name: prometheus-api-cursos
        scrape_interval: 15s
        scrape_timeout: 10s
        metrics_path: /metrics
        scheme: http
        static_configs:
        - targets:
            - prometheus-api-cursos:9090

        - job_name: api-cursos
        metrics_path: /actuator/prometheus
        static_configs:
        - targets:
            # Use "ipconfig" in windows command to get your IP
            # Use "ip addr show" or "ifconfig" if you use MAC or Linux
            - 192.168.15.62:8080
    ```
3. Finally, execute the file 'run.sh' to up the spring boot app, database, grafana and prometheus. You should see an image similar to the one below.
   
   ![Imgurl](docs/images/img-run-all.jpg)

## How to see the Prometheus metrics.

1. Access the url below to see the Spring Boot App metrics.
   
    ```url
    http://localhost:8080/actuator/prometheus
    ```

    You should see an image similar to the one below.

    ![Imgurl](docs/images/img-metrics.jpg)

2. Access the url below to see the Prometheus metrics.
    ```url
    http://localhost:9090/metrics
    ```
    You should see an image similar to the one below.

    ![Imgurl](docs/images/img-prometheus-metrics.jpg)


3. To see the Prometheus console, access the url below.
   ```url
   http://localhost:9090/graph
   ```
   You should see an image similar to the one below.
    
    ![Imgurl](docs/images/img-prometheus.jpg)


## Add data source in Grafana

1. Click on the gear image to go to Grafana settings.
   
   ![Imgurl](docs/images/img-settings.jpg)

2. Click on the button 'Add Data Source', choice the Prometheus kind and fill your data source url. As in the image below.
   
   ![Imgurl](docs/images/img-ds-prometheus.jpg)

   Now, click on the button 'Save and Test'.

   ![Imgurl](docs/images/img-save-test.jpg)


## How to import a dashboard template

1. Click on the button 'Import' on menu 'Dashboard'.

    ![Imgurl](docs/images/img-import-dashboard-template.jpg)

2. Upload the template file. In next page click on the button 'import'.
   
    ![Imgurl](docs/images/img-upload-dashboard-template.jpg)

3. Then You should see the somenthing like as the picture below. If you didn't see, try to stop and to start all aplications.

    ![Imgurl](docs/images/img-dashboard-model.jpg)


## How to up Grafana Loki

1. Create the Grafana Loki container in file 'docker-compose.yml' with the configurations below. Save file and execute the file 'run.sh'.
    ```yml
    loki-api-cursos:
        image: grafana/loki
        container_name: loki-api-cursos
        command:
            - -config.file=/etc/loki/local-config.yaml
            - -print-config-stderr=true
        ports:
            - "3100:3100"
        networks:
            - monit
    ```

## Auto Consume The API

1. Create the client container in file 'docker-compose.yml'

    ```yml
    client-api-cursos:
        build:
            context: ./client/
            dockerfile: Dockerfile
        image: client-api-cursos
        container_name: client-api-cursos
        restart: unless-stopped
        networks:
            - api
    ```